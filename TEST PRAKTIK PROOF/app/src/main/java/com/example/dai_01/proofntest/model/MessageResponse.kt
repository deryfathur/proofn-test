package com.example.dai_01.proofntest.model

data class MessageResponse(
    val `data`: List<DataMessage>
)

data class DataMessage(
    val asPart: Int,
    val attachmentCount: Int,
    val attachments: List<Any>,
    val bcc: List<Any>,
    val box: String,
    val cc: List<Any>,
    val content: String,
    val contentPreview: String,
    val contentType: String,
    val createdAt: String,
    val deliveryStatus: Int,
    val editedAt: Any,
    val externalID: String,
    val firstReadAt: Any,
    val forwardFrom: String,
    val id: String,
    val inReplyTo: String,
    val isGroup: Boolean,
    val isRead: Boolean,
    val labels: List<Any>,
    val lastReadAt: Any,
    val meta: Meta,
    val ownerEmail: String,
    val ownerID: String,
    val participantHash: String,
    val readerList: List<Any>,
    val receivedAt: String,
    val reportCount: Int,
    val sender: Sender,
    val sentAt: String,
    val subject: String,
    val subjectPreview: String,
    val threadID: String,
    val to: List<To>,
    val type: Int,
    val updatedAt: String
)

data class Sender(
    val avatarPathLarge: String,
    val avatarPathMedium: String,
    val avatarPathSmall: String,
    val email: String,
    val firstName: String,
    val fullName: String,
    val hash: String,
    val id: String,
    val lastName: String,
    val slug: String
)

data class To(
    val avatarPathLarge: String,
    val avatarPathMedium: String,
    val avatarPathSmall: String,
    val email: String,
    val fullName: String,
    val hash: String,
    val id: String,
    val phoneNumber: String
)

data class Meta(
    val KV: KV,
    val `data`: Any,
    val draftType: String,
    val id: String,
    val log: Log
)

data class Log(
    val all: String
)

data class DeleteResponse(
        val message: String
)

class KV(
)