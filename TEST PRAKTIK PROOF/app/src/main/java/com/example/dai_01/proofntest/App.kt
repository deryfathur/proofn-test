package com.example.dai_01.proofntest

import android.app.Application
import com.example.dai_01.proofntest.dagger.component.AppComponent
import com.example.dai_01.proofntest.dagger.component.DaggerAppComponent
import com.example.dai_01.proofntest.dagger.module.ApiModule
import com.example.dai_01.proofntest.dagger.module.AppModule
import com.example.dai_01.proofntest.dagger.module.NetworkModule

class App : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder()
                    .appModule(AppModule(this))
                    .networkModule(NetworkModule(" https://beta.proofn.com"))
                    .apiModule(ApiModule())
                    .build()
    }
}