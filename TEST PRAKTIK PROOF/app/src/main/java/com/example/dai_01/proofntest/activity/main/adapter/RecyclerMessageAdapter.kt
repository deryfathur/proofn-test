package com.example.dai_01.proofntest.activity.main.adapter

import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.dai_01.proofntest.R
import com.example.dai_01.proofntest.activity.main.MainActivity
import com.example.dai_01.proofntest.activity.main.MainPresenter
import com.example.dai_01.proofntest.extension.Tools
import com.example.dai_01.proofntest.extension.inflate
import com.example.dai_01.proofntest.model.DataMessage
import java.text.SimpleDateFormat
import java.util.*

class RecyclerMessageAdapter (private val activity: MainActivity, private val presenter: MainPresenter, private val items:List<DataMessage>) : RecyclerView.Adapter<RecyclerMessageAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerMessageAdapter.ViewHolder {
        val inflatedView= parent!!.inflate(R.layout.item_message,false)
        return RecyclerMessageAdapter.ViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerMessageAdapter.ViewHolder?, position: Int) {
        val view=holder?.itemView
        val data=items[position]
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale("id"))
        val newSdf = SimpleDateFormat("dd MMMM yyyy HH:mm", Locale("id"))

        view?.let {

            it.visibility= View.VISIBLE
            (it.findViewById(R.id.tv_subject_message) as TextView).text=data.subject
            (it.findViewById(R.id.tv_content_message) as TextView).text=data.contentPreview

            val date = sdf.parse(data.createdAt)
            (it.findViewById(R.id.tv_time_message) as TextView).text = newSdf.format(date)
        }

        view?.setOnClickListener {
            activity.dialogDetail.findViewById<TextView>(R.id.tv_subject_message_detail).setText(data.subject)
            activity.dialogDetail.findViewById<TextView>(R.id.tv_recipient_message_detail).setText(data.to[0].email)
            activity.dialogDetail.findViewById<TextView>(R.id.tv_content_message_detail).setText(data.contentPreview)

            println("SADASD ${data.contentPreview}")

            val date = sdf.parse(data.createdAt)
            activity.dialogDetail.findViewById<TextView>(R.id.tv_time_message_detail).text = newSdf.format(date)
            activity.dialogAccount.hide()
            activity.dialogDetail.show()

        }

        view?.findViewById<ImageView>(R.id.iv_delete_message)!!.setOnClickListener {
            println("delete")

            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Hapus Pesan Ini ?")
            builder.setPositiveButton("HAPUS", DialogInterface.OnClickListener { dialogInterface, i ->
                presenter.deleteMessage(data.id.toInt())
            })
            builder.setNegativeButton("BATAL", null)
            builder.show()

        }


    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){

    }
}