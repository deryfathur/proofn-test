package com.example.dai_01.proofntest.activity.main

import com.example.dai_01.proofntest.model.DeleteResponse
import com.example.dai_01.proofntest.model.MessageResponse
import com.example.dai_01.proofntest.model.ProfileResponse
import com.example.dai_01.proofntest.mvp.View


interface MainView :View {

   fun onLoadMessageSuccess(message : MessageResponse)

   fun onLoadMessageFailed(message: String)

   fun onDeleteMessageSuccess(message : DeleteResponse)

   fun onDeleteMessageFailed(message: String)

   fun onLoadProfileSuccess(message : ProfileResponse)

   fun onLoadProfileFailed(message: String)


}