package com.example.dai_01.proofntest.activity.main

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.example.dai_01.proofntest.App
import com.example.dai_01.proofntest.R
import com.example.dai_01.proofntest.activity.main.adapter.RecyclerMessageAdapter
import com.example.dai_01.proofntest.model.DataMessage
import com.example.dai_01.proofntest.model.DeleteResponse
import com.example.dai_01.proofntest.model.MessageResponse
import com.example.dai_01.proofntest.model.ProfileResponse
import com.google.gson.GsonBuilder
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerMessageAdapter

    lateinit var dialogDetail: Dialog
    lateinit var dialogAccount: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        App.component.inject(this)

        dialogDetail = Dialog(this)
        dialogDetail.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialogDetail.setContentView(R.layout.layout_detail_message)
        dialogDetail.setCancelable(true)

        dialogAccount = Dialog(this)
        dialogAccount.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialogAccount.setContentView(R.layout.layout_profile)
        dialogAccount.setCancelable(true)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialogDetail.window!!.attributes)
        lp.copyFrom(dialogAccount.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialogDetail.window!!.attributes = lp
        dialogAccount.window!!.attributes = lp

        onAttach()

    }

    override fun onAttach() {

        presenter.onAttach(this)
        presenter.loadMessageList()

        tv_add_message.setOnClickListener {
            Toast.makeText(this,"COMING SOON",Toast.LENGTH_SHORT).show()
        }

        tv_account_message.setOnClickListener {
            presenter.loadProfile()
            dialogAccount.show()
            dialogDetail.hide()
        }

        dialogDetail.findViewById<ImageButton>(R.id.bt_close).setOnClickListener {

            dialogDetail.hide()
        }

        dialogAccount.findViewById<ImageButton>(R.id.bt_close_profile).setOnClickListener {

            dialogAccount.hide()
        }

    }

    private fun initiateRecyclerUpdateView(data:List<DataMessage>) {
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        rv_message.layoutManager = linearLayoutManager
        adapter= RecyclerMessageAdapter(this,presenter,data)
        rv_message.adapter=adapter

    }

    override fun onLoadMessageSuccess(message: MessageResponse) {

        initiateRecyclerUpdateView(message.data)
    }

    override fun onLoadMessageFailed(message: String) {
        println("ERROR GET MESSAGE : $message")
    }

    override fun onDeleteMessageSuccess(message: DeleteResponse) {
        println("SUCCESS DELETE")
        presenter.loadMessageList()
    }

    override fun onDeleteMessageFailed(message: String) {

        println("FAILED DELETE : $message")
    }

    override fun onLoadProfileSuccess(message: ProfileResponse) {
        dialogAccount.findViewById<TextView>(R.id.edit_first_name).setText(message.firstName)
        dialogAccount.findViewById<TextView>(R.id.edit_last_name).setText(message.lastName)
        dialogAccount.findViewById<TextView>(R.id.edit_email).setText(message.email)
        dialogAccount.findViewById<TextView>(R.id.edit_address).setText(message.homeAddress)
    }

    override fun onLoadProfileFailed(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDetach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun lifecycle(): Observable<ActivityEvent> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T> bindTolifeCycle(): LifecycleTransformer<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBackPressed() {
        dialogDetail.dismiss()
        dialogAccount.dismiss()
        finish()
        super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
