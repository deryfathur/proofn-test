package com.example.dai_01.proofntest.extension

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.View
import android.view.WindowManager
import android.widget.TextView

object Tools {

    fun tranparentStatusBar(act: Activity) {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true,act)
        }
        if (Build.VERSION.SDK_INT >= 19) {
            act.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false,act)
            act.window.statusBarColor = Color.TRANSPARENT
        }
    }

    private fun setWindowFlag(bits: Int, on: Boolean, act: Activity) {
        val win = act.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    fun setTextViewHTML(text: TextView, html: String, activity:Activity)
    {
        var sequence: CharSequence = ""
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            //textView.setText(Html.fromHtml(Html.fromHtml(html).toString(), new UrlImageParser(textView, context), null));
            sequence = Html.fromHtml(html)
        }
        val strBuilder = SpannableStringBuilder(sequence);
        val urls = strBuilder.getSpans(0, sequence.length, URLSpan::class.java)
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }


}