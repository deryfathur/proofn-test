package com.example.dai_01.proofntest.activity.main.Login

import android.content.SharedPreferences
import com.example.dai_01.proofntest.dagger.qualifier.Authorized
import com.example.dai_01.proofntest.extension.errorConverter
import com.example.dai_01.proofntest.extension.save
import com.example.dai_01.proofntest.mvp.Presenter
import com.example.dai_01.proofntest.service.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class LoginPresenter@Inject constructor(
        @Authorized val api: ApiService,
        val retrofit: Retrofit,
        val pref: SharedPreferences
): Presenter<LoginView> {

    private var view : LoginView? = null

    var loginDisposables = Disposables.empty()

    override fun onAttach(view: LoginView) {
        this.view = view
    }

    override fun onDetach() {
        view = null
    }

    fun login(loginData:String){

        loginDisposables.dispose()
        val bodySend = RequestBody.create(MediaType.parse("application/json"), loginData)

        loginDisposables = api.login(bodySend)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    res ->
                    pref.save("token", res.token)
                    view?.onLoginSuccess(res)

                }, {
                    err ->
                    if (err is HttpException) {
                        val body = retrofit.errorConverter<Response<Throwable>>(err)
                        view?.onLoginFailed("Error: ${body.errorBody()}")
                    } else {
                        view?.onLoginFailed(err.localizedMessage)
                    }
                })
    }

}