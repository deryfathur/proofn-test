package com.example.dai_01.proofntest.activity.main

import com.example.dai_01.proofntest.dagger.qualifier.Authorized
import com.example.dai_01.proofntest.extension.errorConverter
import com.example.dai_01.proofntest.mvp.Presenter
import com.example.dai_01.proofntest.service.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject
import okhttp3.RequestBody



class MainPresenter @Inject constructor(
        @Authorized val api: ApiService,
        val retrofit: Retrofit
): Presenter<MainView>{

    private var view : MainView? = null

    var messageListDisposables = Disposables.empty()
    var profileDisposables = Disposables.empty()
    var messageDeleteDisposables = Disposables.empty()

    override fun onAttach(view: MainView) {
        this.view = view
    }

    override fun onDetach() {
        view = null
    }

    fun loadMessageList(){

        messageListDisposables.dispose()
        messageListDisposables = api.getMessage()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    res ->

                    view?.onLoadMessageSuccess(res)

                }, {
                    err ->
                    if (err is HttpException) {
                        val body = retrofit.errorConverter<Response<Throwable>>(err)
                        view?.onLoadMessageFailed("Error: ${body}")
                    } else {
                        view?.onLoadMessageFailed(err.localizedMessage)
                    }
                })
    }

    fun deleteMessage(id:Int){

        messageDeleteDisposables.dispose()
        messageDeleteDisposables = api.deleteMessage(id)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    res ->

                    view?.onDeleteMessageSuccess(res)

                }, {
                    err ->
                    if (err is HttpException) {
                        val body = retrofit.errorConverter<Response<Throwable>>(err)
                        view?.onDeleteMessageFailed("Error: ${body}")
                    } else {
                        view?.onDeleteMessageFailed(err.localizedMessage)
                    }
                })
    }

    fun loadProfile(){

        profileDisposables.dispose()
        profileDisposables = api.getProfile()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    res ->

                    view?.onLoadProfileSuccess(res)

                }, {
                    err ->
                    if (err is HttpException) {
                        val body = retrofit.errorConverter<Response<Throwable>>(err)
                        view?.onLoadProfileFailed("Error: ${body}")
                    } else {
                        view?.onLoadProfileFailed(err.localizedMessage)
                    }
                })
    }




}