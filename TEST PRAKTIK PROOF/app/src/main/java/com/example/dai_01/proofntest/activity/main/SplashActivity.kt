package com.example.dai_01.proofntest.activity.main

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.dai_01.proofntest.App
import com.example.dai_01.proofntest.R
import com.example.dai_01.proofntest.activity.main.Login.LoginActivity
import com.example.dai_01.proofntest.extension.Tools
import com.example.dai_01.proofntest.extension.get
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var pref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Tools.tranparentStatusBar(this)

        setContentView(R.layout.activity_splash)

        App.component.inject(this)

        if (pref.get("token") == null) {
            goto(LoginActivity::class.java)
        } else {
            goto(MainActivity::class.java)
        }

    }

    fun <T : Activity> goto(nextClass: Class<T>) {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            val intent = Intent(this, nextClass)
            startActivity(intent)
            finish()
        }, 1500)
    }
}
