package com.example.dai_01.proofntest.model

data class LoginResponse(
    val token: String,
    val user: User
)

data class User(
    val avatarPathLarge: String,
    val avatarPathMedium: String,
    val avatarPathSmall: String,
    val countryDialCode: CountryDialCode,
    val email: String,
    val firstName: String,
    val fullName: String,
    val hasUsablePassword: Boolean,
    val hash: String,
    val id: Int,
    val phoneNumber: String,
    val status: Int,
    val username: String
)

data class CountryDialCode(
    val code: String,
    val dialCode: String,
    val name: String
)

data class LoginRequest(
        val identifier: String,
        val password: String
)