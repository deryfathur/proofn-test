package com.example.dai_01.proofntest.service

import com.example.dai_01.proofntest.model.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiService {

    @GET("/v1/messages/inbox")
    fun getMessage(): Observable<MessageResponse>

    @GET("/v1/user/profile")
    fun getProfile(): Observable<ProfileResponse>

    @POST("/v1/auth/login")
    fun login(@Body body:RequestBody): Observable<LoginResponse>

    @POST("/v1/messages/inbox/{id}/trash")
    fun deleteMessage(@Path("id")id:Int):Observable<DeleteResponse>

}