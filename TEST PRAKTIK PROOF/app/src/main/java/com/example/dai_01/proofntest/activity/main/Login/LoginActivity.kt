package com.example.dai_01.proofntest.activity.main.Login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import com.example.dai_01.proofntest.App
import com.example.dai_01.proofntest.R
import com.example.dai_01.proofntest.activity.main.MainActivity
import com.example.dai_01.proofntest.extension.Tools
import com.example.dai_01.proofntest.model.LoginRequest
import com.example.dai_01.proofntest.model.LoginResponse
import com.google.gson.GsonBuilder
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginView {

    @Inject
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Tools.tranparentStatusBar(this)
        setContentView(R.layout.activity_login)

        App.component.inject(this)

        onAttach()
    }

    override fun onLoginSuccess(message: LoginResponse) {
        //go to Main activity
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onLoginFailed(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }

    override fun onAttach() {

        presenter.onAttach(this)

        login_btn.setOnClickListener {
            val gson = GsonBuilder().setPrettyPrinting().create()
            var username = login_email.text.toString()
            var password = login_password.text.toString()
            var loginObj = LoginRequest(username,password)

            if(username!="" && password!=""){

                val jsonLogin = gson.toJson(loginObj)
                presenter.login(jsonLogin)
            }else{
                Toast.makeText(this,"DATA TIDAK BOLEH KOSONG", Toast.LENGTH_SHORT).show()
            }
        }


    }

    override fun onDetach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun lifecycle(): Observable<ActivityEvent> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T> bindTolifeCycle(): LifecycleTransformer<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
