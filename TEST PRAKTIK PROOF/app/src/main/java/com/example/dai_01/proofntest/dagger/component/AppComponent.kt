package com.example.dai_01.proofntest.dagger.component

import com.example.dai_01.proofntest.activity.main.Login.LoginActivity
import com.example.dai_01.proofntest.activity.main.MainActivity
import com.example.dai_01.proofntest.activity.main.SplashActivity
import com.example.dai_01.proofntest.dagger.module.ApiModule
import com.example.dai_01.proofntest.dagger.module.AppModule
import com.example.dai_01.proofntest.dagger.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        NetworkModule::class,
        ApiModule::class
))

interface AppComponent {

    fun inject(mainActivity:MainActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(loginActivity: LoginActivity)
}