package com.example.dai_01.proofntest.activity.main.Login

import com.example.dai_01.proofntest.model.LoginResponse
import com.example.dai_01.proofntest.mvp.View

interface LoginView :View{
    fun onLoginSuccess(message : LoginResponse)

    fun onLoginFailed(message: String)
}