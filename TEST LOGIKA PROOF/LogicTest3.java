package com.dery.myapplication;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class LogicTest3 {

    public static void min_coins(Integer coin_value[],int n,int amount)
    {
        for( int i=0; i< n; i++ )
            while(amount >= coin_value[i])
            {
                amount= amount - coin_value[i];
                System.out.print(coin_value[i]+" ");
            }
        System.out.print("\n");
    }


    public static void main(String args[])
    {
        int amount = 46;
        Integer[] coin_value= new Integer[]{1,2,4,8};
        Arrays.sort(coin_value, Collections.reverseOrder());
        System.out.println( "The selected currecy values are: ");
        min_coins(coin_value,coin_value.length,amount);
    }

}
