package com.dery.myapplication;

public class LogicTest1 {

    public static void main(String args[]){

        int[] arrNum = new int[]{9,10,8,4,5,3};

        LogicTest1 logicTest1 = new LogicTest1();

        Boolean result = logicTest1.NumbersAdd(15,arrNum);

        System.out.println("RESULT "+result);
    }

    public Boolean NumbersAdd(int x,int[] arr){

        for (int i=0; i<arr.length; i++){

            int diff = x - arr[i];

            for (int j=0; j<arr.length; j++){

                if(arr[j]==diff && j!=i){
                    return true;
                }
            }

        }

        return false;
    }
}
