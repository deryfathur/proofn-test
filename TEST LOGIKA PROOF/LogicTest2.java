package com.dery.myapplication;

import java.util.ArrayList;

public class LogicTest2 {

    public static void main(String args[]){

        int[] arrNum = new int[]{1,-3,5,-2,9,-8,-6,4};

        LogicTest2 logicTest2 = new LogicTest2();

        logicTest2.maxSubArray(arrNum);

    }

    public ArrayList<Integer> maxSubArray(int[] arr){

        int size = arr.length;
        int max = Integer.MIN_VALUE, current = 0;
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            current = current + arr[i];
            if (max < current)
                list.add(arr[i]);
                max = current;
            if (current < 0)
                current = 0;
        }

        System.out.println(list);

        return list;
    }

}
